D4Mobile rocks this world

=============================================

DragonFight - the Game

=============================================

Prologue:
    A long time ago in galaxy far, far away, on the planet Earth lived cute Dragon.
    The Dragon had two children.

    to be continued...

=============================================

Project compilation

=============================================

    Use Android SDK manager:
    
    - Install latest SDK Tools;
    
    - Install latest platform-tools;
    
    - Install build tools v.17;
    
    - Install Android 4.3 (API 18) SDK Platform;
    
    - Install Android support repository and support library;


    Import project to the AndroidStudio as gradle project (use gradle wrapper option)
    or run "gradlew debug" from command line, it will automatically download all gradle files and
    dependency libraries.

=============================================

Repository convention

=============================================

Все пользуем git-flow:

Все публичные методы коммитим с javadoc, также как и необходимо делать описание классов, хотя бы минимально.
Спешки в проекте нету, дедлайна и сроков – тоже. Код коммитить надо максимально чистый, с обязательным соблюдением java code convention.

По разработке:

Все сложные задачи выполняются только на отдельных брэнчах. Брэнч, при готовности, должен отправиться к другому разработчику на ревью в виде pull request-a, и после проведения ревью уже может быть смержен в develop тем, кто делал ревью. 

Брэнчи в репозитории следующие:

- master – сверхстабильный брэнч, туда можно мержить только с develep брэнча и только хорошо оттестированную версию.

- develop – актуальный брэнч разработки, непосредственно на нем можно делать только легкие изменения: поменять стрингу, добавить стили и т.д., все остальные вещи должны делаться на отдельных брэнчах и мержить в него через пулл реквесты.


Префиксы для брэнчей:

Все брэнчи (кроме описанных выше), где будут делаться отдельные таски, должны иметь следующие префиксы (где xxx - имя брэнча с вашими изменениями включающий номер задачи в issue трэкере):

- release/xxx - брэнч с релизами, релиз кандидатами и т.д.;

- feature/xxx - брэнч с новшествами; 

- hotfix/xxx - брэнч с фиксами; 

- refactoring/xxx - брэнч с рефакторингом;

Пример задачи из issue tracker-a с номером 5 в которой имплементируется куб: feature/task_5_implement_cube

Коммиты: 

Все коммиты должны обязательно включать комментарии об изменениях.
