package com.itechart.dragonfight.models;

import javax.microedition.khronos.opengles.GL10;

/**
 * Base opengl object interface
 * Created by Andrey.Sotnikoff on 27.08.13.
 */
public interface GLObject {
    public void draw(GL10 gl);
}
