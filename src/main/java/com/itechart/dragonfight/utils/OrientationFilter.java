package com.itechart.dragonfight.utils;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import java.util.Arrays;

/**
 * Orientation events filter
 * Created by Andrey.Sotnikoff on 27.08.13.
 */
@Deprecated
public class OrientationFilter {

    private static final float kAccelerometerMinStep = 0.033f;
    private static final float kAccelerometerNoiseAttenuation = 3.0f;
    private static final float kSampleRateFastest = 1000.0f/60;    // 0 mSecond
    private static final float kSampleRateGame = 1000.0f/20;       // 20 mSecond
    private static final float kSampleRateUI = 1000.0f/60;         // 60 mSecond
    private static final float kSampleRateNormal = 1000.0f/200;    // 200 mSecond

    private boolean isAdaptive = true;
    private boolean isSampleRateSetByUser = false;
    private float sampleRate = 50;
    private float cutOffFreq = 0.9f;
    private float filterConstant;

    private static final float _zero[] = {0.0f, 0.0f, 0.0f};
    private float _values[];
    private float _accel[];
    private float _gyro[];
    private float _magnetic[];

    private boolean isValuesChanged = false;

    private static final float NS2S = 1.0f / 1000000000.0f;
    private final float[] deltaRotationVector = new float[4];
    private float timestamp;

    public float[] getOrientation() {
        calculateValues();
        return _values.clone();
    }

    public float getAzimuth() {
        calculateValues();
        float result = (float) Math.toDegrees(_values[0]);
        result = (result + 360)%360;
        return result;
    }

    public float getPitch() {
        calculateValues();
        return (float) Math.toDegrees(_values[1]);
    }

    public float getRoll() {
        calculateValues();
        return (float) Math.toDegrees(_values[2]);
    }



    public void calculateValues() {
        if (!isValuesChanged ||
                Arrays.equals(_accel, _zero) ||
                Arrays.equals(_magnetic, _zero)) return;

        isValuesChanged = false;

        float[] rotationMatrix = new float[9];
        float[] remapedRotationMatrix = new float[9];

        if (SensorManager.getRotationMatrix(rotationMatrix, null, _accel, _magnetic)) {
            SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Z, remapedRotationMatrix);
            SensorManager.getOrientation(remapedRotationMatrix, _values);
        }
    }



    public void setSampleRate(int rate) {
        isSampleRateSetByUser = true;
        setSampleRate(getSampleRate(rate));
    }

    private void setSampleRate(float sampleRate) {
        if (sampleRate > 0)
            this.sampleRate = sampleRate;
        calculateAlpha();
    }

    private float getSampleRate(int rate) {
        float result = kSampleRateNormal;
        switch (rate) {
            case SensorManager.SENSOR_DELAY_FASTEST: result = kSampleRateFastest; break;
            case SensorManager.SENSOR_DELAY_GAME: result = kSampleRateGame; break;
            case SensorManager.SENSOR_DELAY_UI: result = kSampleRateUI; break;
            case SensorManager.SENSOR_DELAY_NORMAL: result =  kSampleRateNormal; break;
            default: result = kSampleRateNormal; break;
        }

        return result;
    }

    public void setCutOffFreq(float cutOffFreq) {
        if (cutOffFreq > 0)
            this.cutOffFreq = cutOffFreq;
        calculateAlpha();
    }

    public void setAdaptive(boolean isAdaptive) {
        this.isAdaptive = isAdaptive;
    }

    public OrientationFilter() {
        initFilter(sampleRate, cutOffFreq, isAdaptive);
    }

    public OrientationFilter(int rate) {
        isSampleRateSetByUser = true;
        initFilter(getSampleRate(rate), cutOffFreq, isAdaptive);
    }

    private void initFilter(float rate, float cutOffFreq, boolean isAdaptive) {
        resetFilter();
        isSampleRateSetByUser = true;
        if (sampleRate > 0)
            this.sampleRate = sampleRate;
        if (cutOffFreq > 0)
            this.cutOffFreq = cutOffFreq;

        this.isAdaptive = isAdaptive;

        calculateAlpha();
    }

    public void resetFilter() {
        _accel = _zero.clone();
        _magnetic = _zero.clone();
        _gyro = _zero.clone();
        _values = _zero.clone();
    }





    public static float norm(float[] v) {
        float d = 0;
        for (float value: v) {
            d += value*value;
        }

        return (float) Math.sqrt(d);
    }

    public static float norm(float x, float y) {
        return (float) Math.sqrt(x*x +y*y);
    }

    public static float norm(float x, float y, float z) {
        return (float) Math.sqrt(x*x + y*y + z*z);
    }

    public static float clamp(float v, float min, float max) {
        if (v > max) return max;
        else if (v < min) return min;
        else return v;
    }

    private void calculateAlpha() {
        // high pass filter
        float RC = 1.0f / cutOffFreq;
        float dt = 1.0f / sampleRate;
        filterConstant = RC / (dt + RC);
    }

    private void lpf(float[] v, float[] _v, float filterConstant, float resolution, float noiseAttenuation, boolean isAdaptive) {
        // Mark that orientation has been changed
        isValuesChanged = true;

        // Initialize filtered values with current
        if (Arrays.equals(_v, _zero)) {
            _v[0] = v[0];
            _v[1] = v[1];
            _v[2] = v[2];
            return;
        }

        double alpha = filterConstant;

        if (isAdaptive) {
            double d = clamp(Math.abs(norm(_v[0], _v[1], _v[2]) - norm(v[0], v[1], v[2])) / resolution - 1.0f, 0.0f, 1.0f);
            alpha = (1.0f - d) * filterConstant / noiseAttenuation + d * filterConstant;
        }

        _v[0] = (float) (v[0] * alpha + _v[0] * (1.0f - alpha));
        _v[1] = (float) (v[1] * alpha + _v[1] * (1.0f - alpha));
        _v[2] = (float) (v[2] * alpha + _v[2] * (1.0f - alpha));
    }




    public void addEvent(SensorEvent event) {
        if (event == null || event.sensor == null) return;

        int sensorType = event.sensor.getType();
        switch (sensorType) {
            case Sensor.TYPE_ACCELEROMETER: addAccelerometerEvent(event); break;
            case Sensor.TYPE_GYROSCOPE: addGyroscopeEvent(event); break;
            case Sensor.TYPE_MAGNETIC_FIELD: addMagneticFieldEvent(event); break;
            default: break;
        }
    }

    /**
     * Add Accelerometer sensor sample
     * Uses low pass filter to compensate noise
     * @param event - sensorEvent
     */
    private void addAccelerometerEvent(SensorEvent event) {
        if (event.values == null || event.values.length != 3) return;
        lpf(event.values, _accel, filterConstant, event.sensor.getResolution(), kAccelerometerNoiseAttenuation, isAdaptive);
    }

    /**
     * Add MagneticField sensor sample
     * Uses low pass filter to compensate noise
     * @param event - sensorEvent
     */
    private void addMagneticFieldEvent(SensorEvent event) {
        if (event.values == null || event.values.length != 3) return;
        lpf(event.values, _magnetic, filterConstant, event.sensor.getResolution(), kAccelerometerNoiseAttenuation, isAdaptive);
    }

    /**
     * Add Gyroscope sensor sample
     * Uses low pass filter to compensate noise
     * @param event - sensorEvent
     */
    private void addGyroscopeEvent(SensorEvent event) {
        if (event.values == null || event.values.length != 3) return;

        isValuesChanged = true;
/*
        // This timestep's delta rotation to be multiplied by the current rotation
        // after computing it from the gyro sample data.
        if (timestamp != 0) {
            final float dT = (event.timestamp - timestamp) * NS2S;
            // Axis of the rotation sample, not normalized yet.
            float axisX = event.values[0];
            float axisY = event.values[1];
            float axisZ = event.values[2];

            // Calculate the angular speed of the sample
            float omegaMagnitude = norm(axisX, axisY, axisZ);

            // Normalize the rotation vector if it's big enough to get the axis
            if (omegaMagnitude > EPSILON) {
                axisX /= omegaMagnitude;
                axisY /= omegaMagnitude;
                axisZ /= omegaMagnitude;
            }

            // Integrate around this axis with the angular speed by the timestep
            // in order to get a delta rotation from this sample over the timestep
            // We will convert this axis-angle representation of the delta rotation
            // into a quaternion before turning it into the rotation matrix.
            float thetaOverTwo = omegaMagnitude * dT / 2.0f;
            float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
            float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
            deltaRotationVector[0] = sinThetaOverTwo * axisX;
            deltaRotationVector[1] = sinThetaOverTwo * axisY;
            deltaRotationVector[2] = sinThetaOverTwo * axisZ;
            deltaRotationVector[3] = cosThetaOverTwo;
        }
        timestamp = event.timestamp;
        float[] deltaRotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);
        // User code should concatenate the delta rotation we computed with the current rotation
        // in order to get the updated rotation.
        // rotationCurrent = rotationCurrent * deltaRotationMatrix;
        */
    }



}
