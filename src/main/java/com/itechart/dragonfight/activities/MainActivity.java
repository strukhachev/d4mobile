package com.itechart.dragonfight.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;

import com.itechart.dragonfight.R;
import com.itechart.dragonfight.views.GlLayerView;
import com.itechart.dragonfight.views.CameraView;

import java.util.List;

/**
 * MainActivity
 * Created by Andrey.Sotnikoff on 17.09.13.
 */
public class MainActivity extends Activity{
    /** The OpenGL view */
    private GlLayerView mGLView;
    private RelativeLayout mRelativeLayer;

    private CameraView mCameraView;

    final int EGL_COVERAGE_BUFFERS_NV = 0x30E0;
    final int EGL_COVERAGE_SAMPLES_NV = 0x30E1;

    private SensorManager sensorManager;
    private LocationManager locationManager;

    private Sensor rotationVectorSensor;




    // Listener for gps location provider events
    private LocationListener locationListener = new LocationListener() {
        public double latitude;
        public double longitude;
        public double altitude;
        public double accuracy;

        // Location Event listener
        @Override
        public void onLocationChanged(Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            altitude = location.getAltitude();
            accuracy = location.getAccuracy();
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };


    // Listener for sensor events (rotationVector sensor)
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                int[] axes = getScreenAxes();

                float[] rotationMatrix = new float[16];
                SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values);

                float[] remappedRotationMatrix = new float[16];
                SensorManager.remapCoordinateSystem(rotationMatrix, axes[0], axes[1], remappedRotationMatrix);
                mGLView.setRotationMatrix(remappedRotationMatrix);
            }
        }
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        LayoutInflater inflater = getLayoutInflater();

        // Next, we disable the application's title bar...
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // ...and the notification bar. That way, we can use the full screen.
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );


        // Check device for OpenGL ES 2.0 compatibility
        if (hasGLES20()) {
            mGLView = new GlLayerView(this);
            mCameraView = new CameraView(this);

            //getWindow().setContentView(mGLView);
            getWindow().addContentView(mGLView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            getWindow().addContentView(mCameraView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        } else {
            showText(getString(R.string.low_opengl_version));
        }

        mRelativeLayer = (RelativeLayout) inflater.inflate(R.layout.activity_main, null);
        //getWindow().setContentView(mRelativeLayer);
        getWindow().addContentView(mRelativeLayer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // Find and Initialize rotationVector sensor needed to determine orientation
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (rotationVectorSensor == null) {
            showText(getString(R.string.required_sensor_is_not_found));
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    }


    @Override
    protected void onResume() {
        super.onResume();
        // Resume the glSurface
        if (mGLView != null) {
            mGLView.onResume();
        }

        // Start listening for rotation events
        if (rotationVectorSensor != null)
            sensorManager.registerListener(sensorEventListener, rotationVectorSensor, SensorManager.SENSOR_DELAY_FASTEST);

        // Startl istening fro location updates
        if (locationManager != null)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Also pause the glSurface
        if (mGLView != null) {
            mGLView.onPause();
        }

        // Stop listening for rotation events
        if (rotationVectorSensor != null)
            sensorManager.unregisterListener(sensorEventListener, rotationVectorSensor);

        // Stop listening for location updates
        if (locationManager != null)
            locationManager.removeUpdates(locationListener);
    }


    /**
     * Show Text message to the User with text
     * @param text - message text
     */
    private void showText(String text) {
        TextView mTextView = new TextView(this);
        mTextView.setText(text);
        getWindow().addContentView(mTextView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
    }

    /**
     * Check device for OpenGlES 2.0 compaibility
     * @return true if device supports ES2.0, false - otherwise
     */
    private boolean hasGLES20() {
        ActivityManager am = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        return info.reqGlEsVersion >= 0x20000;
    }

    /**
     * In case device can be rotated (landscape, portrait, reverse...) we have to
     * remap rotation matrix and change our device coordinate system
     * @return - X and Y axe for current device orientation
     */
    private int[] getScreenAxes() {
        int x = sensorManager.AXIS_X;
        int y = sensorManager.AXIS_Y;

        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_90:
                x = SensorManager.AXIS_Y;
                y = SensorManager.AXIS_MINUS_X;
                break;
            case Surface.ROTATION_180:
                x = SensorManager.AXIS_MINUS_X;
                y = SensorManager.AXIS_MINUS_X;
                break;
            case Surface.ROTATION_270:
                x = SensorManager.AXIS_MINUS_Y;
                y = SensorManager.AXIS_X;
                break;
            case Surface.ROTATION_0:
            default:
                break;
        }

        int[] axes = {x, y};
        return axes;
    }

}
