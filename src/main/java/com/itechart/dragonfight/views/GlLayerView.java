package com.itechart.dragonfight.views;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.GLSurfaceView.Renderer;
import android.view.SurfaceHolder;

import com.itechart.dragonfight.models.GLCoordinateSystem;
import com.itechart.dragonfight.models.GLCube;

/**
 * DragonCube gl object
 * Created by Andrey.Sotnikoff on 17.09.13.
 */
public class GlLayerView extends GLSurfaceView implements SurfaceHolder.Callback, Renderer {

    int onDrawFrameCounter = 1;

    Context context;
    GLCube cube;
    GLCoordinateSystem GLCoordinateSystem;

    private float screenWidth;
    private float screenHeight;
    private float screenAspect;
    private float[] rotationMatrix = new float[16];

    public GlLayerView(Context c) {
        super(c);
        context = c;

        this.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        this.setRenderer(this);
        this.getHolder().setFormat(PixelFormat.TRANSLUCENT);

        cube = new GLCube();
        GLCoordinateSystem = new GLCoordinateSystem();

        // Set Identity matrix
        rotationMatrix[0] = 1;
        rotationMatrix[4] = 1;
        rotationMatrix[8] = 1;
        rotationMatrix[12] = 1;
    }

    public void onDrawFrame(GL10 gl) {
        onDrawFrameCounter++;
        // Clear color and depth buffers using clear-value set earlier
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        // Draw Coordinate system
        gl.glMatrixMode(GL10.GL_PROJECTION); // Select projection matrix
        gl.glPushMatrix();                   // Save current projection matrix
        gl.glLoadIdentity();                 // Reset projection matrix
        gl.glOrthof(0.0f, screenAspect, 0.0f, 1.0f, 1.0f, -1.0f);
        gl.glMatrixMode(GL10.GL_MODELVIEW);  // Select model-view matrix
        gl.glPushMatrix();                   // Save current model-view matrix
        gl.glLoadIdentity();                 // Reset model-view matrix

        // Draw coordinate system
        gl.glTranslatef(0.1f, 0.1f, 0.1f);
        gl.glScalef(0.1f, 0.1f, 1.0f);

        gl.glMultMatrixf(rotationMatrix, 0);
        GLCoordinateSystem.draw(gl);


        gl.glPopMatrix();
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();                 // Reset the mode-view matrix

        // Rotate camera according sensors positions
        gl.glMultMatrixf(rotationMatrix, 0);

        //gl.glRotatef(rotationVector[0], rotationVector[1], rotationVector[2], rotationVector[3]);
        //gl.glRotatef(azimuth, 0.0f, 1.0f, 0.0f);
        //gl.glRotatef(pitch, 1.0f, 0.0f, 0.0f);
        //gl.glRotatef(roll, 0.0f, 0.0f, 1.0f);
        //GLU.gluLookAt(gl, 0, 0, 0, 0, 0, -1, 0, 1, 0); // Look at the North

        // draw our object
        //gl.glRotatef(85, 1.0f, 0.0f, 0.0f);
        //gl.glRotatef(85, 1.0f, 1.0f, 0.0f);

        //Draw Cube
        gl.glTranslatef(0.0f, -7.0f, 0.0f);  // Translate right and into the screen
        cube.draw(gl);                   // Draw cube
    }

    /**
     * Set camera projection matrix and viewport
     * @param gl - gl10
     * @param width - new Screen width
     * @param height - new Screen Height
     */
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        screenWidth = width;
        screenHeight = height;

        gl.glViewport(0, 0, width, height);

        if (height == 0)
            height = 1;   // To prevent divide by zero
        screenAspect = (float) width / height;

        // Setup perspective projection, with aspect ratio matches viewport
        gl.glMatrixMode(GL10.GL_PROJECTION); // Select projection matrix
        gl.glLoadIdentity();                 // Reset projection matrix
        // Use perspective projection
        GLU.gluPerspective(gl, 45, screenAspect, 0.1f, 100.f);

        gl.glMatrixMode(GL10.GL_MODELVIEW);  // Select model-view matrix
        gl.glLoadIdentity();                 // Reset
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);  // Set color's clear-value to black
        gl.glClearDepthf(1.0f);            // Set depth's clear-value to farthest
        gl.glEnable(GL10.GL_DEPTH_TEST);   // Enables depth-buffer for hidden surface removal
        gl.glDepthFunc(GL10.GL_LEQUAL);    // The type of depth testing to do
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);  // nice perspective view
        gl.glShadeModel(GL10.GL_SMOOTH);   // Enable smooth shading of color
        gl.glDisable(GL10.GL_DITHER);      // Disable dithering for better performance

        //mosquito.loadTextureFromResource(gl, context, R.drawable.mosquito);
    }

    /**
     * Set rotationMatrix (regarding to the current device rotation)
     * @param matrix - rotation matrix
     */
    public void setRotationMatrix(float[] matrix) {
        rotationMatrix = matrix.clone();
    }

}
