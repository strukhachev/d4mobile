package com.itechart.dragonfight.views;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.itechart.dragonfight.activities.MainActivity;

import java.io.IOException;
import java.util.List;

/**
 * Created by mac-249 on 8/14/13.
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {
    Camera mCamera = null;
    boolean isPreviewRunning = false;

    Context mContext;

    public CameraView(MainActivity context) {
        super(context);
        mContext = context;
        SurfaceHolder mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        synchronized(this) {

            if(mCamera == null){
                mCamera = Camera.open();

                if (mCamera == null) {
                    // no front-facing camera, use the first back-facing camera instead.
                    // you may instead wish to inform the user of an error here...
                    mCamera = Camera.open(0);
                }

                Camera.Parameters p = mCamera.getParameters();
                List<Camera.Size> previews = p.getSupportedPreviewSizes();

                Display display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                int width;
                int height;

                if(android.os.Build.VERSION.SDK_INT >= 13){
                    Point size = new Point();
                    display.getSize(size);
                    width = size.x;
                    height = size.y;
                } else{
                    width = display.getWidth();
                    height = display.getHeight();
                }

                int previewSizeIndex = 0;
                int currentHeight = 0;
                for(int i = 0; i<previews.size(); i++){
                    if(previews.get(i).height <= height &&
                            currentHeight < previews.get(i).height){
                        previewSizeIndex = i;
                        currentHeight = previews.get(i).height;
                    }
                }
                Camera.Size currentSize = previews.get(previewSizeIndex);
                p.setPreviewSize(currentSize.width,currentSize.height);
                mCamera.setParameters(p);

                try {
                    mCamera.setPreviewDisplay(holder);
                } catch (IOException e) {
                    Log.e("Camera", "mCamera.setPreviewDisplay(holder);");
                }

                mCamera.startPreview();
            }
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        // Because the CameraDevice object is not a shared resource, it's very
        // important to release it when the activity is paused.
        synchronized(this) {
            try {
                if (mCamera!=null) {
                    mCamera.stopPreview();
                    isPreviewRunning=false;
                    mCamera.release();
                    mCamera = null;
                }
            } catch (Exception e) {
                Log.e("Camera", e.getMessage());
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

    }

    public void onPreviewFrame(byte[] arg0, Camera arg1) {

    }
}
